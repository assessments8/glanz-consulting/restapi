import express from 'express'
import dotenv from 'dotenv'
import mongoose from 'mongoose'
import cookieParser from 'cookie-parser'
import cors from 'cors'

import authRoutes from './routes/authRoutes.js'
import userRoutes from './routes/userRoutes.js'

const PORT = 8800

//instantiate express application
const app = express()

//loads environment variables from a . env file into process. env
dotenv.config()

//initial connection to connect to mongoDB
const connect = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI)
    console.log('MongoDB Connection Successfull!')
  } catch (error) {
    throw error
  }
}

//catch diconnected event and log a message
mongoose.connection.on('disconnected', () => {
  console.log('mongoDB disconnected!')
})

mongoose.connection.on('connected', () => {
  console.log('mongoDB Connected!')
})

app.use(cookieParser()) //will accept/parse the token as cookie
app.use(express.json()) //will allow sending json object to express
app.use(cors({ origin: 'http://localhost:3000', credentials: true }))

app.use('/api/auth', authRoutes)
app.use('/api/users', userRoutes)

//handle all routes error. made possible by route next() function
app.use((err, req, res, next) => {
  const errorStatus = err.status || 500
  const errorMessage = err.message || 'Something went wrong!'
  return res.status(errorStatus).json({
    success: false,
    status: errorStatus,
    message: errorMessage,
    stack: err.stack,
  })
})

app.listen(PORT, () => {
  connect()
  console.log(`Backend Connection to Port ${PORT} Successfull.`)
})

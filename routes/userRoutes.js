import express from 'express'
import {
  getUser,
  getUsers,
  updateUser,
  deleteUser,
  updatePassword,
} from '../controllers/userController.js'
import { verifyAdmin, verifyToken, verifyUser } from '../utils/verifyJwt.js'

const router = express.Router()

/**TEST ROUTES MIDDLEWARES
 * verifyToken
 * verifyUser
 * verifyAdmin */
router.get('/jwtcheck', verifyToken, (req, res, next) => {
  res.send('token verified!')
})
router.get('/checkuser/:id', verifyUser, (req, res, next) => {
  res.send('You are logged in!')
})
router.get('/checkadmin', verifyAdmin, (req, res, next) => {
  res.send('you are admin!')
})

//CREATE
//handled by authRoutes REGISTER

//READ ONE
router.get('/:id', verifyUser, getUser)

//READ ALL
router.get('/', verifyAdmin, getUsers)

//UPDATE
router.put('/:id', verifyUser, updateUser)

//UPDATE PASSWORD
router.put('/updatepassword/:id', verifyUser, updatePassword)

//DELETE
router.delete('/:id', verifyUser, deleteUser)

export default router
